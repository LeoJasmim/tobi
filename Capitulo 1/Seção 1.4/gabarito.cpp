#include <cstdio>
#include <cstdlib>

int main(){
	int i, n, acertos = 0;
	char gabarito[81], prova[81];
	scanf("%d", &n);
    scanf("%s", gabarito);
    scanf("%s", prova);
	for(i=0;i<n;i++){
        if(gabarito[i] == prova[i]){
            acertos++;
        }
	}
	printf("%d\n", acertos);
    return 0;
}
