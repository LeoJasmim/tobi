#include <cstdio>
#include <cstdlib>

int main(){
	int i, n, l, c, s=0;
	scanf("%d", &n);
	for(i=1; i<=n; i++){
        scanf("%d %d", &l, &c);
        if(l > c){
            s+=c;
        }
	}
	printf("%d\n", s);
    return 0;
}
