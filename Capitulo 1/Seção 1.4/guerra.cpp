#include <cstdio>
#include <cstdlib>

int main(){
	int i, n, secoes[100000], meio, total = 0, busca = 0;

	scanf("%d", &n);

	for (i=0; i<n; i++){
        scanf("%d", &secoes[i]);
        total += secoes[i];
	}

	meio = total/2;

	for (i=0; i<n; i++){
        busca += secoes[i];
        if(busca == meio){
            printf("%d\n", i+1);
            break;
        }
    }
    return 0;
}
