#include <cstdio>
#include <cstdlib>
#include <cstring>


int main(){
    int i, n, p, s=0, maior=0, menor=1000000;

    int *a;
    a = (int*) malloc(1000000*sizeof(int));
    memset(a,0,1000000*sizeof(int));

    scanf("%d", &n);

    for(i=1; i<=n; i++){
        scanf("%d", &p);
        a[p] = 1;
        if(p>maior){
            maior = p;
        }
        if(p<menor){
            menor = p;
        }
    }

    for(i=menor; i<=maior; i++){
        s+=a[i];
    }

    printf("%d\n", s);

    return 0;
}

