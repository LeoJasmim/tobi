#include <cstdio>

int main(){
	int cv, ce, cs, fv, fe, fs, c, f;
	scanf("%d %d %d %d %d %d", &cv, &ce, &cs, &fv, &fe, &fs);
	c = (3*cv) + ce;
	f = (3*fv) + fe;
	
	if (c == f)	{
		if (cs > fs){
			printf ("C");
		} else if (cs < fs){
			printf ("F");
		} else {
			printf ("=");
		}
	} else if (c > f){
		printf ("C");
	} else {
		printf ("F");
	}
	return 0;
}
