#include <cstdio>

int max (int x, int y){
	if (x>y)
		return x;
	else
		return y;
}

int main(){
	int a, b, c;
	scanf("%d %d %d", &a, &b, &c);
	if (a > b && a > c){
		printf("%d\n", max(b,c));			
	} else if (b > a && b > c){
		printf("%d\n", max(a,c));			
	} else {
		printf("%d\n", max(a,b));			
	}	
	return 0;
}
