#include <cstdio>

int min (int a, int b, int c){
	if (a<=b && a<=c)
		return a;
	else{
		if (b<=a && b<=c)
			return b;
		else
			return c;
	}	
}

int main(){
	int a, b, c;
	scanf("%d %d %d", &a, &b, &c);
	printf ("%d\n", min(int(a/2),int(b/3),int(c/5)));
	return 0;
}
 
