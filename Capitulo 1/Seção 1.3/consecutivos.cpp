#include <cstdio>
#include <cstdlib>

int main(){
	int i, n, s=1, m=1;
	int *v;
	
	scanf("%d", &n);
	v = (int*) malloc (n*sizeof(int));
		
	for (i=0;i<n;i++){
		scanf("%d", &v[i]);		
	}
	
	for (i=0;i<n-1;i++){
		if (v[i] == v[i+1]){
			s++;
		} 
		else{
			if(s > m){
				m = s;
			}		
			s = 1;	
		}
	}
	if(s > m){
		m = s;
	}
	printf ("%d\n", m);
	return 0;	
}
