#include <cstdio>
#include <cstdlib>

void bubbleSortUp(float vet[], int n){
     int i, j;
     float aux;
     for (i=0; i<n-1; i++){   
         for (j=i+1; j<n; j++)
             if (vet[i] > vet [j]){
                aux=vet[i];
                vet[i]=vet[j];
                vet[j]=aux;
             }   
	}
}

int main(){
	int i;
	float soma = 0;
	float notas[5];
	for(i=0;i<5;i++){
		scanf("%f", &notas[i]);
	}
		
	bubbleSortUp(notas,5);
		
	for(i=1; i<4;i++){
		soma += notas[i];
	}
		
	printf("%.1f\n", soma);
	
	return 0;
}
