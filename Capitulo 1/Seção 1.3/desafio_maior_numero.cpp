#include <cstdio>
#include <cstdlib>

int main(){
	int i, n, m;
	scanf("%d", &n);
	m = n;
	while (n != 0){
		if(n > m)
			m = n;
		scanf("%d", &n);
	}
	printf("%d\n", m);

	return 0;	
}
