#include <cstdio>
#include <cstdlib>

int main(){
	int L, c=1;	
	scanf("%d", &L);
	do{
		c *= 4;
		L = L/2;
	} while(L > 1);
	printf("%d\n", c);
	return 0;
}
