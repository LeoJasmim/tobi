#include <cstdio>
#include <cstdlib>

int main(){
	int i, n, total = 0;
	char *c;
	scanf("%d", &n);
	c = (char*) malloc (n*sizeof(char));
	scanf("%s", c);

	for (i=0; i<n; i++){	
		switch (c[i]){
			case 'P':
				total+=2;
				break;
			case 'C':
				total+=2;
				break;
			case 'A':
				total+=1;
				break;
			default:
				break;
		}		
	}
	printf ("%d\n", total);	
	return 0;
}
