#include <cstdio>
#include <cstdlib>

int main(){
	int i, n, c, s, e, total=0;	
	scanf("%d %d", &n, &c);
	for (i=1; i<=n; i++){
		scanf("%d %d", &s, &e);
		total -= s;
		total += e;
		if (total > c){
			printf("S\n");
			return 0;
		}
	}
	printf ("N\n");
	return 0;	
}
