#include <cstdio>
#include <cstdlib>
#include <algorithm>

#define MAX 100

using namespace std;

int paises[MAX];
int index_paises[MAX];

bool compara(int x, int y){
    if(paises[x] > paises[y]){
        return true;
    }
    else{
        if(paises[x] < paises[y])
            return false;
        else{
            if(x <= y)
                return true;
            else
                return false;
        }
    }
}

int main(){

	int i, o, p, b, m, n;

	scanf("%d %d", &n, &m);

	for(i=0;i<n; i++){
        index_paises[i]=i;
	}

	for(i=0; i<m; i++){
        scanf("%d %d %d", &o, &p, &b);
        paises[o-1] += 1;
        paises[p-1] += 1;
        paises[b-1] += 1;
	}

	sort(index_paises, index_paises+n, compara);

	for(i=0;i<n; i++){
        printf ("%d ", index_paises[i]+1);
	}

    return 0;
}
