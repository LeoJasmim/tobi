#include <cstdio>
#include <cstdlib>
#include <algorithm>

#define MAX 100000

using namespace std;

int casas[MAX];

int main(){
	int i, j, n, k, a;

	scanf("%d", &n);

	for (i=0; i<n; i++){
        scanf("%d", &casas[i]);
	}

	scanf("%d", &k);

    for(i=0; i<n; i++){
        if(casas[i] < k){
            a = k-casas[i];
            if(binary_search(casas+i, casas+n, a)){
                printf("%d %d", casas[i], a);
                return 0;
            }
        }
    }
    return 0;
}
