#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <algorithm>

#define MAXN 100
#define MAXM 10001

using namespace std;

int tempo[MAXM][MAXN];
int id[MAXN];
int k,n,m;

bool compara(int x, int y){
    if(tempo[m][x] > tempo[m][y]){
        return true;
    }
    else{
        if(tempo[m][x] < tempo[m][y]){
            return false;
        }
        else{
            int i = m-1;
            while (i >= 0){
                if(tempo[i][x] > tempo[i][y]){
                    return true;
                }
                else{
                    if(tempo[i][x] < tempo[i][y])
                        return false;
                }
                i--;
            }
        }
    }
}
