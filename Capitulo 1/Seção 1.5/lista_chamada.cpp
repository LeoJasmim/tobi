#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>

#define MAX 101
#define MAXNOME 21

using namespace std; // algorithm

int aluno[MAX];
char nome[MAX][MAXNOME];

bool compara(int x, int y){
	if(strcmp(nome[x],nome[y]) < 0)
        return true;
    else
        return false;
}

int main(){

	int n, i, k;
	scanf("%d %d", &n, &k);

	for(i=0; i<n; i++){
        aluno[i] = i;
        scanf("%s", nome[i]);
	}

	sort(aluno, aluno+n, compara);

    printf("%s\n", nome[aluno[k-1]]);
	return 0;
}
