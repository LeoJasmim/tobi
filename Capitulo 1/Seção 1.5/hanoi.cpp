#include <cstdio>
#include <cstdlib>
#include <cmath>

int main(){
    int n, t=1;
    double b,e;
	scanf("%d", &n);
	while(n != 0){
        b = 2;
        e = (double) n;
        printf("Teste %d\n%ld\n\n", t, (long int) (pow(b,e)-1));
        t++;
        scanf("%d", &n);
	}
    return 0;
}
