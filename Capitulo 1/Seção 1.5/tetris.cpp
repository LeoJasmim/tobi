#include <cstdio>
#include <cstdlib>
#include <algorithm>
#include <cstring>

#define MAX 1000
#define MAXNOME 16

using namespace std; // algorithm

int jogador[MAX];
int pontos[MAX][13];
char nome[MAX][MAXNOME];
int teste = 1;
int posicao[MAX];

bool compara(int x, int y){
	if(pontos[x][12] > pontos[y][12])
        return true;
    else{
        if(pontos[x][12] < pontos[y][12])
            return false;
        else{
            if(strcmp(nome[x],nome[y]) < 0)
                return true;
            else
                return false;
        }
    }
}

int main(){
    int i, j, n;
    scanf("%d", &n);
    while(n != 0){
        for(i=0; i<n; i++){
            jogador[i] = i;
            scanf("%s", nome[i]);
            for(j=0; j<12; j++){
                scanf("%d", &pontos[i][j]);
            }
        }

        for(i=0;i<n;i++){
            sort(pontos[i], pontos[i]+12);
            pontos[i][12] = 0;
            for(j=1; j<=10; j++){
                pontos[i][12] += pontos[i][j];
            }
        }

        sort(jogador, jogador+n, compara);

        posicao[0] = 1;

        for(i=1; i<n; i++){
            if(pontos[jogador[i]][12] == pontos[jogador[i-1]][12]){
                posicao[i] = posicao[i-1];
            }
            else{
                posicao[i] = i+1;
            }
        }

        printf("Teste %d\n", teste);
        for(i=0; i<n; i++){
            printf("%d %d %s\n", posicao[i], pontos[jogador[i]][12], nome[jogador[i]]);
        }
        printf("\n");
        teste++;
        scanf("%d", &n);
    }
    return 0;
}
