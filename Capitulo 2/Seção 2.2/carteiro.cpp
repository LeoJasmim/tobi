#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <iostream>
#include <string>
#include <algorithm>
#include <set>
#include <map>

#define MAX 45000

using namespace std;

int main(){

    int i, j, n, m, k, a, b, d = 0;

    map <int,int> casas;
    map <int,int>::iterator it;
    int rota[MAX];

    cin >> n >> m;

    for (i=1; i<=n; i++){
        cin >> k;
        casas[k] = i;
    }

    for (i=0; i<m; i++){
        cin >> k;
        rota[i] = k;
    }

    //primeira casa
    it = casas.begin();
    a = (*it).second;

    //primeira casa da rota
    for(i=0; i<m; i++){
        it = casas.find(rota[i]);
        b = (*it).second;
        d += (int)abs(a-b);
        a = b;
    }

    cout << d << "\n";

    return 0;
}
