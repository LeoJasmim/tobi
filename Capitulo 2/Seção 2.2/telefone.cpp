#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <string>
#include <iostream>
#include <algorithm>
#include <set>
#include <map>

#define MAX 16

using namespace std;

int main(){

    int i;

    string alfa;
    string num;

    map<char,char>::iterator it;
    map<char,char> teclas;

    teclas['A'] = '2';
    teclas['B'] = '2';
    teclas['C'] = '2';
    teclas['D'] = '3';
    teclas['E'] = '3';
    teclas['F'] = '3';
    teclas['G'] = '4';
    teclas['H'] = '4';
    teclas['I'] = '4';
    teclas['J'] = '5';
    teclas['K'] = '5';
    teclas['L'] = '5';
    teclas['M'] = '6';
    teclas['N'] = '6';
    teclas['O'] = '6';
    teclas['P'] = '7';
    teclas['Q'] = '7';
    teclas['R'] = '7';
    teclas['S'] = '7';
    teclas['T'] = '8';
    teclas['U'] = '8';
    teclas['V'] = '8';
    teclas['W'] = '9';
    teclas['X'] = '9';
    teclas['Y'] = '9';
    teclas['0'] = '0';
    teclas['1'] = '1';
    teclas['2'] = '2';
    teclas['3'] = '3';
    teclas['4'] = '4';
    teclas['5'] = '5';
    teclas['6'] = '6';
    teclas['7'] = '7';
    teclas['8'] = '8';
    teclas['9'] = '9';
    teclas['-'] = '-';

    cin >> alfa;

    for (i=0; i<alfa.length(); i++){
        it = teclas.find(alfa[i]);
        if(it != teclas.end())
            cout << (*it).second;
    }

    cout << "\n";

    return 0;
}

