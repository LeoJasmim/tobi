#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <string>
#include <cmath>
#include <iostream>
#include <algorithm>
#include <set>
#include <map>

using namespace std;

int main(){

    int i, n;
    double x, s = 0;
    string nome;

    map <string,double> alunos;
    map <string,double>::iterator it;

    cin >> n;

    while (n > 0){

        cin >> nome;

        for (i=0; i<4; i++){
            cin >> x;
            if(alunos.find(nome) == alunos.end())
                alunos[nome] = x;
            else
                alunos[nome] += x;
        }

        n--;
    }

    for(it = alunos.begin(); it != alunos.end(); it++){
        cout << "Aluno: " << (*it).first << " - ";
        cout << "Média Final: " << ((*it).second / 4) << "\n";
    }

    return 0;
}
