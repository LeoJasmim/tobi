#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <set>

using namespace std;

int main(){

    int i, n;

    pair<set<int>::iterator,bool> ret;

    set<int> conjunto;
    set<int> temp;

    set<int>::iterator it;
    set<int>::iterator itlow;
    set<int>::iterator itup;


    scanf("%d", &n);

    for (i=0; i<n; i++){
        conjunto.insert(i);
    }

    temp = conjunto;

    itlow = temp.lower_bound(0);
    itup = temp.upper_bound(0);
    temp.erase(itlow, itup);


    for (it = temp.begin(); it!= temp.end(); it++){
        printf ("%d ", *it);
    }

    printf("\n");
    printf("diff: %d\n", conjunto.size() - temp.size());

    for (i=0; i<n*2; i++){
        if (i%2 == 0)
            if (conjunto.count(i)){
                printf("%d Localizado\n", i);
                conjunto.erase(i);
                if (!conjunto.count(i))
                    printf("%d Apagado\n", i);
            }
    }

    for (it = conjunto.begin(); it!= conjunto.end(); it++){
        printf ("%d ", *it);
    }

    printf("\nsize: %d\n", conjunto.size());
    conjunto.clear();
    printf("size: %d\n", conjunto.size());

    //retorno do insert devolve um ponteiro para
    //o valor e bool se foi possível inserir ou não
    ret = conjunto.insert(5);
    printf("%d %d\n", *ret.first, ret.second);
    ret = conjunto.insert(5);
    printf("%d %d\n", *ret.first, ret.second);
    conjunto.clear();

    return 0;
}
