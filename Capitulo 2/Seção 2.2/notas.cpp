#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <iostream>
#include <string>
#include <algorithm>
#include <set>
#include <map>

using namespace std;

int main(){

    int i, n, k, x, c = 0, m = -1;
    multiset<int> notas;
    multiset<int>::iterator it;

    cin >> n;

    for (i=0; i<n; i++){
        cin >> x;
        notas.insert(x);
    }

    for (it = notas.begin(); it != notas.end(); it++){
        n = (*it);
        k = notas.count(n);
        if (k > c || (k == c && n > m)){
            c = k;
            m = n;
        }
    }

    cout << m << "\n";

    return 0;
}
