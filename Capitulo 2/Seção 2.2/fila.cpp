#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <iostream>
#include <string>
#include <algorithm>
#include <set>
#include <map>

#define MAX 50000

using namespace std;

int main(){

    int i, n, m, k;
    int ordem[MAX];
    set<int> fila;
    set<int>::iterator it;

    cin >> n;
    for(i=0; i<n; i++){
        cin >> k;
        ordem[i] = k;
        fila.insert(k);
    }

    cin >> m;

    for(i=0; i<m; i++){
        cin >> k;
        fila.erase(k);
    }

    for(i=0; i<n; i++){
        if(fila.count(ordem[i])){
            cout << ordem[i] << " ";
        }
    }

    cout << "\n";
    return 0;
}
