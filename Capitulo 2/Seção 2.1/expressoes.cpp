#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <stack>

#define MAX 100001

using namespace std;

char cadeia[MAX];
char pilha[MAX];
int top = -1;

bool push(char x){
    if(top < MAX){
        pilha[++top] = x;
        return true;
    }
    else{
        return false;
    }
}

bool isEmpty(){
    if(top == -1)
        return true;
    return false;
}

bool pop(){
    if(isEmpty()){
        return false;
    }
    else{
        top--;
        return true;
    }
}

char topo(){
    if(isEmpty()){
        return NULL;
    }
    else{
        return pilha[top];
    }
}

void clearPilha(){
    top = -1;
}

bool isCaracterCorreto(char cc, char cp){
    if(cc == ')' && cp == '('){
        return true;
    }else{
        if(cc == '}' && cp == '{'){
            return true;
        }
        else{
            if(cc == ']' && cp == '['){
                return true;
            }
            else{
                return false;
            }
        }
    }
}


int main(){

    int t, i, j;

    scanf("%d", &t);

    bool isValida = true;

    for(i=0; i<t; i++){
        scanf("%s", cadeia);

        if(strlen(cadeia)%2 != 0){
            isValida = false;
        }
        else {
            for(j=0; j<strlen(cadeia); j++){
                if(cadeia[j] == '(' || cadeia[j] == '{' || cadeia[j] == '['){
                    push(cadeia[j]);
                }
                else{
                    if(isEmpty()){
                        isValida = false;
                        break;
                    }
                    else{
                        if(isCaracterCorreto(cadeia[j],topo())){
                            pop();
                        }
                        else{
                            isValida = false;
                            break;
                        }
                    }
                }
            }
        }

        if(!isEmpty() || !isValida){
            printf("N\n");
        }else{
            printf("S\n");
        }

        clearPilha();
        isValida = true;
    }

    return 0;
}
