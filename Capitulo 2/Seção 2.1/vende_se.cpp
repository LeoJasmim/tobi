#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <algorithm>

#define MAX 100000
#define INF 1000001

using namespace std;

int predios[MAX];

int main(){

    int i, n, k, menor, d, x;

    scanf("%d %d", &n, &k);

    for(i=0; i<n; i++){
        scanf("%d", &predios[i]);
    }

    sort(predios,predios+n);

    menor = INF;
    d = n-k-1;

    for(i=0; (i+d) < n; i++){
        x = predios[i+d]-predios[i];

        if(x < menor){
            menor = x;
        }
    }

    printf("%d\n", menor);
    return 0;
}
