#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <algorithm>

#define MAXN 10000
#define MAXT 1000

using namespace std;

struct Aluno{
    char nome[50];
    int habil;
};

Aluno times[MAXT][MAXN];
int top[MAXT];

Aluno alunos[MAXN];
int idxHabil[MAXN];

Aluno time[MAXN];
int idxNome[MAXN];

bool pushPilha(Aluno a, int t){
    if(top[t] < MAXN){
        top[t]++;
        times[t][top[t]] = a;
        return true;
    }
    else{
        return false;
    }
}

bool popPilha(Aluno *a, int t){
    if(top[t] == -1){
        a = NULL;
        return false;
    }
    else{
        *a = times[t][top[t]];
        top[t]--;
        return true;
    }
}

bool testeHabil(int x, int y){
    if(alunos[x].habil > alunos[y].habil)
        return true;
    else
        return false;
}

bool testeNome (int x, int y){
	if(strcmp(time[x].nome,time[y].nome) < 0)
        return true;
    else
        return false;
}

int main(){

    int i, j, n, t, k, cont=1;

    Aluno aux;

    scanf("%d %d", &n, &t);

    for(i=0; i<n; i++){
        scanf("%s %d", &alunos[i].nome, &alunos[i].habil);
        idxHabil[i] = i;
    }

    sort(idxHabil,idxHabil+n,testeHabil);

    for(i=0; i<t; i++){
        top[i] = -1;
    }

    k = 0;
    while(k < n){
        i = k%t;
        pushPilha(alunos[idxHabil[k]],i);
        k++;
    }

    k = 0;

    for(i=0; i<t; i++){
        while(popPilha(&aux,i)){
            time[k] = aux;
            idxNome[k] = k;
            k++;
        }
        sort(idxNome,idxNome+k,testeNome);

        printf("Time %d\n", cont);
        for(j=0; j<k;j++){
            printf("%s\n", time[idxNome[j]].nome);
        }
        printf("\n");

        k = 0;
        cont++;
    }

    return 0;
}
