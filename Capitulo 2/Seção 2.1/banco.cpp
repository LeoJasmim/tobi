#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <algorithm>
#include <queue>

#define MAXN 1000
#define MAXC 10

using namespace std;

struct Cliente{
    int t;
    int d;
};

int idx[MAXC];
int caixas[MAXC];

bool compara(int x, int y){
    if(caixas[x] < caixas[y])
        return true;
    else
        return false;
}

int main(){

    int c, n, t, d, i, j;

    int foraDoPrazo = 0;

    Cliente aux;
    queue <Cliente> clientes;

    scanf("%d %d", &c, &n);

    for(i=0;i<c;i++){
        caixas[i] = 0;
        idx[i] = i;
    }

    for(i=0; i<n; i++){
        scanf("%d %d", &aux.t, &aux.d);
        clientes.push(aux);
    }

    while(!clientes.empty()){
        aux = clientes.front();
        clientes.pop();

        caixas[idx[0]] = max(caixas[idx[0]],aux.t);

        if(caixas[idx[0]] - aux.t > 20){
            foraDoPrazo++;
        }

        caixas[idx[0]] += aux.d;

        sort(idx,idx+c,compara);
    }

    printf("%d\n", foraDoPrazo);

    return 0;
}
